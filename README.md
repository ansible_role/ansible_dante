## Dante ansible role

This role is intented for [dante server](https://www.inet.no/dante/) 
installation from sources.


### Requirements
 - ansible >= 2.4.0
 
 
### Installation
```bash
git clone https://bitbucket.org/p-app/ansible_dante.git
cd ansible_dante
ansible-galaxy install -r requirements.yml
```


### Usage
You should prepare inventory file and playbook file.

*Example inventory: inventory.ini*
```ini
[dante]
my-dante-server.example.com

[dante:vars]
ansible_user = root
ansible_ssh_private_key_file = /local/path/to/private/ssh/key/id_rsa
```

*Example playbook: playbook.yml*
```yaml

- hosts: dante
  roles:
    - ../ansible_dante

  vars:
    dante_interface_internal:

      - interface: "{{ ansible_default_ipv4.interface }}"
        port: 1080

    dante_rule_client:
      - comment: allow only my external ip
        from: 1.2.3.4/32

    dante_rule_server:

      - comment: bind outgoing traffic
        command: bind connect udpassociate

      - comment: incoming connections/packets
        command: bindreply udpreply
```

Then, you should run playbook as ansible requires.
```bash
ansible-playbook -i inventory.ini playbook.yml -vv
```
